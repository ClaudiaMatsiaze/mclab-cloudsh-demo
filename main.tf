provider "oci" {
  region = var.region
}

# Creating n-Boot Volume using the boot volume backup
resource "oci_core_boot_volume" "boot_volumes" {
  count          = var.instance_count
  availability_domain = var.availability_domain
  compartment_id = var.compartment_id
  display_name = "${var.boot_volume_display_name}-${count.index}"

   source_details {
        # Required
        id = var.boot_volume_backup_source_id
        type = var.boot_volume_backup_source_type
    }
}

# Creating n-Instances and attaching the boot volume we created
resource "oci_core_instance" "instances" {
  count          = var.instance_count
  availability_domain = var.availability_domain
  compartment_id = var.compartment_id
  display_name   = "${var.instance_display_name}-${count.index}"
  shape = var.instance_shape

    create_vnic_details {
      subnet_id      = var.subnet_id
      display_name = "${var.vnic_display_name}-${count.index}"
    }

    shape_config {
      memory_in_gbs = var.instance_shape_config_memory_in_gbs
      ocpus = var.instance_shape_config_ocpus
    }

    source_details {
      # Required
      source_id =  oci_core_boot_volume.boot_volumes[count.index].id
      source_type = "bootVolume"
    }
}
