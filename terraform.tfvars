# Global vars
region = "eu-frankfurt-1"
availability_domain = "fcxI:EU-FRANKFURT-1-AD-1"
compartment_id = "ocid1.compartment.oc1..aaaaaaaavzxsiufyb5vml546so35gibsqpl3xvd3v5d3xsu3nikam2jevnva"

# Boot Volume Backup Source Details
boot_volume_backup_source_id = "ocid1.bootvolumebackup.oc1.eu-frankfurt-1.abtheljslljvybzyioapzfnnwzw3pkvdyzhsrlibonlsduuovorun4ibn7gq"
boot_volume_backup_source_type = "bootVolumeBackup"

## (Note: Change the 4 last digits in all display names with the current MMYY: 0223 = Fevrier 2023)
# Boot Volume 
boot_volume_display_name = "BOOT_VOLUME_0223"

# Instances
instance_count = 2
instance_shape = "VM.Standard.E4.Flex"
instance_display_name ="PSFT_FSCM_0223"
instance_shape_config_ocpus = "1"
instance_shape_config_memory_in_gbs = "16"

vnic_display_name = "psft-fscm-0223"
subnet_id = "ocid1.subnet.oc1.eu-frankfurt-1.aaaaaaaacanb3wmuqekd7abznfpg3yfluvn5cjhejbbl6ooam5rvbarmlskq"
